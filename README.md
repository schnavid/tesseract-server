# Installation

- `git clone https://gitlab.com/schnavid/tesseract-server.git && cd tesseract-server`
- `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`
- `source $HOME/.cargo/env`
- `sudo apt install pkg-config gcc libssl-dev clang libleptonica-dev libtesseract-dev tesseract-ocr-deu`
- `cargo build --release`
- In `.env` die richtigen Werte für SSL einstellen (Private Key, Chain, Hostname, und `TESS_SSL` auf `true` sonst läuft der Server nur auf `localhost`)
- In `tesseractserver.service` die richtigen Werte für den Pfad zur binary (`./target/release/tesseract-server`) und Working Dir (`./`) einstellen
- Service Datei starten
