use actix_web::{web, Error, App, HttpServer, HttpResponse, Responder};
use actix_files::NamedFile;
use actix_multipart::Multipart;
use actix_cors::Cors;
use std::io::Write;
use futures::StreamExt;
use uuid::Uuid;
use serde::ser::{Serializer, SerializeStruct, self};
use serde::{Deserialize, Serialize};
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};

async fn form() -> impl Responder {
    NamedFile::open("public/index.html")
}

#[derive(Debug, Serialize, Deserialize)]
struct FileInfo {
    pub created: usize,
    pub modified: usize,
    pub size: usize,
}

#[derive(Debug, Serialize, Deserialize)]
struct Response {
    pub image_name: String,
    pub text_name: String,
    pub text: String,
    pub file_info: FileInfo,
}

struct MyResult(Result<Response, String>);

impl ser::Serialize for MyResult {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        let mut state = serializer.serialize_struct("Result", 2)?;
        match &self.0 {
            Ok(r) => {
                state.serialize_field("type", "success")?;
                state.serialize_field("message", &r)?;
            }
            Err(e) => {
                state.serialize_field("type", "error")?;
                state.serialize_field("message", &e)?;
            }
        }
        state.end()
    }
}

async fn fileupload(mut payload: Multipart) -> Result<HttpResponse, Error> {
    // iterate over multipart stream
    if let Some(item) = payload.next().await {
        let mut field = item.unwrap();

        if field.content_type().type_() != mime::IMAGE {
            return Ok(HttpResponse::BadRequest().json(MyResult(Err("expected image".to_owned()))))
        }

        // let content_type = field.content_disposition().unwrap();
        // let filename = content_type.get_filename().unwrap();
        let filename = Uuid::new_v4();
        let filepath = format!("./files/{}-img", filename);
        let filepath1 = filepath.clone();
        let filepath2 = format!("./files/{}-txt", filename);
        // File::create is blocking operation, use threadpool
        let mut f = web::block(|| std::fs::File::create(filepath1))
            .await
            .unwrap();
        // Field in turn is stream of *Bytes* object
        while let Some(chunk) = field.next().await {
            let data = chunk.unwrap();
            // filesystem operations are blocking, we have to use threadpool
            f = web::block(move || f.write_all(&data).map(|_| f)).await?;
        }

        let mut lt = leptess::LepTess::new(None, "deu").unwrap();
        lt.set_image(filepath.as_str());
        let text = lt.get_utf8_text().unwrap();
        
        let metadata = std::fs::metadata(format!("files/{}-img", filename)).unwrap();

        let file_info = FileInfo {
            size: metadata.len() as usize,
            created: metadata.created().unwrap().duration_since(std::time::SystemTime::UNIX_EPOCH).map(|x| x.as_secs() as usize).unwrap_or(0),
            modified: metadata.modified().unwrap().duration_since(std::time::SystemTime::UNIX_EPOCH).map(|x| x.as_secs() as usize).unwrap_or(0),
        };

        let response = Response {
            image_name: format!("{}-img", filename),
            text_name: format!("{}-txt", filename),
            text: text.clone(),
            file_info
        };
        let response = HttpResponse::Ok().json(MyResult(Ok(response)));

        let mut f = web::block(|| std::fs::File::create(filepath2))
            .await
            .unwrap();

        web::block(move || f.write_all(text.bytes().collect::<Vec<u8>>().as_slice())).await?;

        Ok(response)
    } else {
        Ok(HttpResponse::BadRequest().json(MyResult(Err("expected file".to_owned()))))
    }
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();

    std::fs::create_dir_all("./tmp").unwrap();

    let app = HttpServer::new(|| {
        App::new()
            .wrap(
                Cors::new()
                    .allowed_methods(vec!["GET", "PATCH", "PUT", "POST", "DELETE", "OPTIONS"])
                    //.allowed_headers(vec![http::header::ACCEPT, http::header::CONTENT_TYPE, http::header::ORIGIN])
                    .send_wildcard()
                    .finish()
            ).service(
            web::resource("/fileupload")
                .route(web::get().to(form))
                .route(web::post().to(fileupload))
        )
    });

    let hostname = std::env::var("TESS_HOSTNAME").unwrap();

    if std::env::var("TESS_SSL").unwrap() == "true" {
        let privkey = std::env::var("TESS_SSL_PRIVATE_KEY").unwrap();
        let chain = std::env::var("TESS_SSL_CHAIN").unwrap();

        let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
        builder
            .set_private_key_file(privkey, SslFiletype::PEM)
            .unwrap();
        builder
            .set_certificate_chain_file(chain)
            .unwrap();

        app.server_hostname(hostname.clone())
            //.bind("136.243.10.234:80")?
            .bind_openssl(hostname + ":443", builder)?
            //.bind("tesseract.hosting-pilot.com:80")?
            //.bind("127.0.0.1:8080")?
            .run()
            .await
    } else {
        app.server_hostname(hostname)
            //.bind("136.243.10.234:80")?
            //.bind_openssl("tesseract.hosting-pilot.com:443", builder)?
            //.bind("tesseract.hosting-pilot.com:80")?
            .bind("127.0.0.1:8080")?
            .run()
            .await
    }

}
